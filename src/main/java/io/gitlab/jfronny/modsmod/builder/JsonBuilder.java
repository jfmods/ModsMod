package io.gitlab.jfronny.modsmod.builder;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GPrefer;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.modsmod.ModsMod;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class JsonBuilder {
    public static void build(int number, Path outputPath, boolean parent) throws IOException {
        try (BufferedWriter bw = Files.newBufferedWriter(outputPath)) {
            GC_JsonBuilder.ModManifest.serialize(new ModManifest(number, parent), bw, LibJf.JSON_TRANSPORT);
        }
    }

    @GSerializable
    public record ModManifest(int schemaVersion, String id, String version, String name, Map<String, ModMenuData> custom) {
        @GPrefer
        public ModManifest {
        }

        public ModManifest(int number, boolean parent) {
            this(1, "modmod_" + number, "1.0", "ModsMod " + number, parent ? Map.of("modmenu", new ModMenuData("modsmod")) : Map.of());
        }

        @GSerializable
        public record ModMenuData(String parent) { }
    }
}
