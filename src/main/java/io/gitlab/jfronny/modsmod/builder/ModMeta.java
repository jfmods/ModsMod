package io.gitlab.jfronny.modsmod.builder;

import net.fabricmc.loader.impl.util.FileSystemUtil;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.Path;

public class ModMeta {
    public final Path rootPath;
    public final Path fmj;
    public final URL url;

    public ModMeta(Path f) throws IOException {
        this(FileSystemUtil.getJarFileSystem(f, false).get(), f);
    }

    public ModMeta(FileSystem fs, Path f) throws MalformedURLException {
        rootPath = fs.getPath("");
        fmj = getFmj(fs);
        this.url = f.toUri().toURL();
    }

    public static Path getFmj(FileSystem fs) {
        return fs.getPath("fabric.mod.json");
    }
}
