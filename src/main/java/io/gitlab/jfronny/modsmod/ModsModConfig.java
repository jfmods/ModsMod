package io.gitlab.jfronny.modsmod;

import io.gitlab.jfronny.libjf.config.api.v2.Entry;
import io.gitlab.jfronny.libjf.config.api.v2.JfConfig;

@JfConfig
public class ModsModConfig  {
    @Entry public static boolean parent = false;
    @Entry public static boolean cache = true;
    @Entry public static int modsCount = 26;

    static {
        JFC_ModsModConfig.ensureInitialized();
    }
}
