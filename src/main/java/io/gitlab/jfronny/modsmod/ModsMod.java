package io.gitlab.jfronny.modsmod;

import io.gitlab.jfronny.commons.logger.SystemLoggerPlus;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigInstance;
import io.gitlab.jfronny.libjf.unsafe.UltraEarlyInit;
import io.gitlab.jfronny.modsmod.builder.JsonBuilder;
import io.gitlab.jfronny.modsmod.builder.ModMeta;
import io.gitlab.jfronny.modsmod.util.IOUtil;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.impl.FabricLoaderImpl;
import net.fabricmc.loader.impl.metadata.*;
import net.fabricmc.loader.impl.util.FileSystemUtil;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.HashSet;

public class ModsMod implements UltraEarlyInit {
    static final HashSet<ModMeta> MODS = new HashSet<>();
    static final FabricLoaderImpl LOADER = FabricLoaderImpl.INSTANCE;
    public static final String MOD_ID = "modsmod";
    public static final SystemLoggerPlus LOGGER = SystemLoggerPlus.forName(MOD_ID);
    private static final String CACHE_NAME = MOD_ID + "cache";

    @Override
    public void init() {
        try {
            JFC_ModsModConfig.ensureInitialized();
            //Load config
            Path modsmodCfgFile = ConfigInstance.get(MOD_ID).getFilePath().orElseThrow();
            //make sure the modsmodcache dir is ok
            Path path = LOADER.getConfigDir().resolve(CACHE_NAME);
            if (!Files.isDirectory(path)) {
                if (Files.exists(path))
                    Files.delete(path);
                Files.createDirectories(path);
            }
            //remove modsmodcache if the cache is outdated
            try {
                Path cfgCache = path.resolve("_basecfg");
                if (Files.exists(cfgCache)) {
                    if (Files.isRegularFile(cfgCache)) {
                        if (!IOUtil.contentEquals(modsmodCfgFile, cfgCache)) {
                            IOUtil.clearDirectory(path);
                        }
                    }
                    else {
                        IOUtil.clearDirectory(path);
                    }
                }
                Files.copy(modsmodCfgFile, cfgCache, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                LOGGER.error("Failed to validate modsmod config cache, caching will not be available", e);
            }
            MODS.clear();
            //Generate mods
            for (int i = 0; i < ModsModConfig.modsCount; i++) {
                Path f = path.resolve("f" + (i + 1) + ".jar");
                boolean exists = Files.exists(f);
                //Do not load if cached
                if (exists) {
                    if (ModsModConfig.cache) {
                        MODS.add(new ModMeta(f));
                        continue;
                    } else Files.delete(f);
                }
                try (FileSystem fs = FileSystemUtil.getJarFileSystem(f, true).get()) {
                    Path inf = fs.getPath("META-INF");
                    Files.createDirectory(inf);
                    Files.writeString(inf.resolve("MANIFEST.MF"), "Manifest-Version: 1.0\n");
                    JsonBuilder.build(i + 1, ModMeta.getFmj(fs), ModsModConfig.parent);
                }
                MODS.add(new ModMeta(f));
                System.gc();
            }
            FabricLoaderInterface.synchronize(LOADER);
        } catch (IOException e) {
            LOGGER.error("Could not create modsmod mods", e);
        }
    }

    public static void loadMods() {
        for (ModMeta meta : MODS) {
            try {
                LoaderModMetadata info = ModMetadataParser.parseMetadata(Files.newInputStream(meta.fmj),
                        meta.url.toString(),
                        new ArrayList<>(),
                        new VersionOverrides(),
                        new DependencyOverrides(FabricLoader.getInstance().getConfigDir()),
                        false);
                FabricLoaderInterface.addMod(LOADER, FabricLoaderInterface.createPlain(meta.rootPath, info, false, new HashSet<>()));
            } catch (IOException | ParseMetadataException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
