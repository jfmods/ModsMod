ModsMod generates and loads a selectable number of empty mods.

This is done using LibJF.

You can find the config at .minecraft/config/ModsMod.json5 or in ModMenu